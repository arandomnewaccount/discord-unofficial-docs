# discord-unoffical-docs
https://arandomnewaccount.gitlab.io/discord-unofficial-docs/lazy_guilds.html 
Unofficial documentation for the Discord API.

## Building

```bash
npm i
npm run build
```

HTML Output will be in the `/public` folder.

## Developing

```bash
npm run start
```
