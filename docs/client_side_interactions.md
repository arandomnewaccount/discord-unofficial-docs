# Client-side Interactions

## OP 24 'Request Interactions'
Used by the client to retreive a list of interactions. Gateway responds with GUILD_APPLICATION_COMMANDS_UPDATE event.

| field | type | description |
| --: | :-- | :-- |
| `applications` | boolean               | false when using slash commands (documentation needed)
| `guild_id`     | snowflake             | ID of guild to query interactions for
| `limit`        | integer               | How many interactions to return
| `nonce`        | string                | Nonce, used for the event returning the data
| `offset`       | integer               | 0-indexed offset for retrieving interactions
|`query`         | string                | case-insensitive command name search
|`command_ids`   | list[ApplicationID]  | list of application ids

## `GUILD_APPLICATION_COMMANDS_UPDATE` event
Returns the data for OP 24
\* note, if the limit was set to a low number (~10), a duplicate `GUILD_APPLICATION_COMMANDS_UPDATE` is received. 

| field | type | description |
| --: | :-- | :-- |
| `application_commands` | list[ApplicationCommand] | list of application commands |
| `applications`         | list[Application]        | list of applications         |
| `guild_id`             | snowflake                | the same id used in OP 24    |
| `nonce`                | string                   | the same nonce used in OP 24 |
| `updated_at`           | unix timestamp           | when the interactions were updated. this is not relative to the discord epoch |

### Application
| field | type | description |
| --: | :-- | :-- |
| `bot`           | Bot       | partial user structure of the bot
| `command_count` | integer   | how many application commands does this application have
| `icon`          | string    | avatar hash of the application
| `id`            | snowflake | id of the application
| `name`          | string    | name of the application

#### Bot
| field           | type      | description
| --:             | :--       | :--
| `avatar`        | string    | avatar hash
| `bot`           | boolean   | always true
| `discriminator` | string    | discriminator of the bot
| `id`            | snowflake | id of the bot
| `public_flags`  | integer   | flags of the bot (check discord official docs)
| `username`      | string    | username of the bot

### ApplicationCommand
| field                | type                               | description
| --:                  | :--                                | :--
| `application_id`     | string                             | id of the app who owns this command
| `default_permission` | boolean                            | whether the command is enabled by default in this guild
| `description`        | string                             | description of this command (1-100 characters)
| `id`                 | snowflake                          | id of this command
| `name`               | string                             | name of this command (1-32 lowercase characters)
| `permissions`        | list[[ApplicationCommandPermission]](https://discord.com/developers/docs/interactions/slash-commands#applicationcommandpermissions)  | the permissions for the command in the guild
| `version`            | snowflake                          | ??? (documentation needed)

## POST `/interactions`
Use an interaction. Takes a InteractionBody and responds with 204. Also causes gateway events INTERACTION_CREATE and INTERACTION_SUCCESS/INTERACTION_FAILURE to be received.
\* note, for slash command triggering, multipart/form-data, name="payload_json" is used
### InteractionBody
| field                | type                               | description
| --:                  | :--                                | :--
| `application_id`     | snowflake                          | id of application to use command for
| `channel_id`         | snowflake                          | id of the channel of the interaction
| `data`               | InteractionBodyData                | [data of the interaction](#interactionbodydata)
| ?`guild_id`          | snowflake                          | if interaction was in a guild, its id
| `nonce`              | string                             | nonce for responding in interaction create and success events
| `type`               | integer                            | ??? (documentation needed) with slash commands, type = 2

#### InteractionBodyData
##### Button/Dropdown clicking
_Buttons:_
| field                | type                               | description
| --:                  | :--                                | :--
| `component_type`     | integer                            | type = 2
| `custom_id`          | string                             | custom id, found in the message components

_Dropdowns:_
| field                | type                               | description
| --:                 | :--                                 | :--
| `component_type`    | integer                             | type = 3
| `custom_id`         | string                              | custom id, found in the message components
| `values`            | list[dropdownValue]                 | for example, ["jpg", "png"]. Dropdown option values are found in the message components

##### Slash command triggering
| field                | type                               | description
| --:                  | :--                                | :--
| `type`               | integer                            | type = 1
| `version`              | string                           | version of command
| `id`                   | string                           | command id
| `name`                 | string                           | command name
| `options`              | list[command]                    | [command options](#command-options)
| `attachments`          | list[attachments]                | hasn't been implemented yet into discord's api

###### _command options_:
| field                | type                               | description
| --:                  | :--                                | :--
| `type`               | integer                            | command type
| `name`               | string                             | command name
| ?`value`             | string                             | value, if 3 ≤ type ≤ 10
| ?`options`           | list[command]                      | command options, only used if type == 1 or type == 2

###### _command types_:
| type      | name                | value  
| --:       | :--                 | :--
| 1         | SUB_COMMAND         | n/a
| 2         | SUB\_COMMAND\_GROUP   | n/a
| 3        | STRING               | string
| 4        | INTEGER              | integer
| 5        | BOOLEAN              | boolean
| 6        | USER                 | snowflake
| 7        | CHANNEL              | snowflake
| 8        | ROLE                 | snowflake
| 9        | MENTIONABLE          | snowflake
| 10       | NUMBER               | number

###### example data:
<img src="https://gitlab.com/arandomnewaccount/discord-unofficial-docs/-/raw/master/docs/pictures/slashcmd.png"  width="400" height="135">          

```json
{
  "version": "880232136341101400",
  "id": "824059358388289578",
  "name": "saved",
  "type": 1,
  "options": [
    {
      "type": 2,
      "name": "queues",
      "options": [
        {
          "type": 1,
          "name": "create",
          "options": [
            {
              "type": 3,
              "name": "name",
              "value": "test"
            }
          ]
        }
      ]
    }
  ],
  "attachments": []
}
```

## `INTERACTION_CREATE` event
Responds to /interactions.
data: InteractionResponseBody

## `INTERACTION_SUCCESS` event
Sent after INTERACTION_CREATE if the event was successful.
data: InteractionResponseBody

## `INTERACTION_FAILED` event
Sent after INTERACTION_CREATE if the event failed.
data: InteractionResponseBody

### InteractionResponseBody
| field   | type      | description
| --:     | :--       | :--
| `id`    | snowflake | maybe id of this event (???) (documentation needed)
| `nonce` | snowflake | nonce in POST /interactions
