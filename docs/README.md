# discord-unofficial-docs

Unofficial documentation for some Undocumented Discord APIs.

## Topics

 - [Lazy Guilds](/lazy_guilds.html)
 - [Client Side Interactions](/client_side_interactions.html)

## Licensing?

![CC0 logo](https://i.creativecommons.org/p/zero/1.0/88x31.png)

To the extent possible under law, [Luna Mendes](https://l4.pm) has waived all
copyright and related or neighboring rights to the Unofficial Discord API
Documentation. This work is published from Brazil.

edited some pages - arandomnewaccount
